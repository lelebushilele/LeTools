 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun.utils;

 import org.junit.jupiter.api.Test;
 import cn.lele.fun.utils.MiddleController.*;

// import static cn.lele.fun.utils.MiddleController.showBubbleSort;
 import static cn.lele.fun.utils.MiddleController.*;

 /**
  * <p>Project: LeTools - UtilTest
  * <p>Powered by wuyahan On 2023-01-12 21:30:32
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class UtilTest {


     @Test
     public void testTrans() {
         MiddleController.getWordsChineseOrEnglish("hellofsdfdsgasfwerg");
     }

     @Test
     public void testSearch() {

         MiddleController.getAllInThisWord("智慧树网");

     }

     @Test
     public void testDoc() {
         MiddleController.createJavaDocByFileLocations("F:\\desk\\周测\\542教学辅助系统\\LeTools\\src\\main\\java\\cn\\lele\\fun\\utils\\MiddleController.java");
     }

     @Test
     public void test3() {
         MiddleController.showArrayLitSource();
     }

     @Test
     public void test5() {
//         showBubbleSort();
     }

     @Test
     public void test() {

     }

     @Test
     public void test6() {
         System.out.println(guessNumber());
     }

     @Test
     public void test43(){
        showEachSort(SortEnum.BINARY);
     }

     @Test
     public void test16(){
         showNumbMultiplyNumb(18);
     }

     @Test
     public void test12(){
         showBeauty();
     }

     @Test
     public void test124(){
         System.out.println("getLocalTime() = " + getLocalTime());
     }

     @Test
     public void test123(){
         showHashMapDemo();
     }

 }
