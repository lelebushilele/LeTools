 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun.utils;

 /**
  * <p>Project: LeTools - MiddleController
  * <p>Powered by wuyahan On 2023-01-12 20:34:11
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */

 import org.jsoup.Connection;
 import org.jsoup.Jsoup;
 import org.jsoup.nodes.Document;
 import org.jsoup.nodes.Element;
 import org.jsoup.select.Elements;

 import java.io.*;
 import java.time.LocalDateTime;
 import java.util.Arrays;
 import java.util.Random;

 /** 中心方法 总控全局
  * <p>Powered by wuyahan on 2023-01-12 20:34</p>
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since
  */
 public class MiddleController {

     /*
      * Description: 翻译软件 增强区分
      * @date: 2023/1/12 21:41
      * @params
      * @param word:
      * @return java.lang.String
      */
     public static String getWordsChineseOrEnglish(String word) {
         String ydUrl = "https://youdao.com/result?word=%s&lang=en";
         // 获取连接对象
         Connection connection = Jsoup.connect(String.format(ydUrl, word)).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36");
         connection.followRedirects(true);
         connection.ignoreContentType(true);
         String text = "";
         try {
             // 正常情况下的翻译区域
             text = connection.get().getElementsByClass("basic").get(0).text();
             // 英文翻译
             if (text.matches("[A-Za-z]+")) {
                 text = text.replaceAll("\\s\\d", "\r\n$0");
             } else {
                 // 非英文翻译
                 text = text.replace("；", "\r\n");
             }
             return text;
         } catch (Exception e) {
             System.out.println("您输入是否有误？小的搜不到啊~");
         }
         return text;
     }

     /*
      * Description: 搜索尽可能搜索到的 这个先放弃 无法绕过验证
      * @date: 2023/1/12 21:41
      * @params
      * @param word:
      * @return java.lang.String
      */
     public static String getAllInThisWord(String word) {
         // 连接地址
         String bdUrl = "https://baike.baidu.com/item/%s";
         // 获取连接对象
//         Connection connection = Jsoup.connect(String.format(bdUrl,word)).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36").followRedirects(true).ignoreContentType(true);
         Connection connection = Jsoup.connect(String.format(bdUrl, word));
         // 做伪装
//         connection.header("Host","baike.baidu.com");
//         connection.header("Referer",String.format(bdUrl,word));
//         connection.header("","");
         // 获取内容
         try {

             Document document = connection.get();
             System.out.println("document = " + document);

             Elements elements = connection.get().select("div[class=lemma-summary]");
             for (Element element : elements) {
                 System.out.println("element = " + element);
             }
//             System.out.println("text = " + text);
         } catch (IOException e) {
             throw new RuntimeException(e);
         }

         return "";
     }

     /**
      * 生成java说明文档
      * @param fileLocation
      */
     public static void createJavaDocByFileLocations(String fileLocation) {
         if (!fileLocation.endsWith("java")) {
             System.out.println("对不起输入的地址不正确");
             return;
         }
         File file = new File(fileLocation);
         if (file.isDirectory() || !file.exists()) {
             System.out.println("请检查您输入的地址是否有误~");
             return;
         }

         File dir = new File(System.getProperty("user.dir") + File.separator + "api");
         if (!dir.exists()) {
             dir.mkdirs();
         }

         System.out.println(dir.getAbsolutePath());

         // 执行命令生成文档
         try {
             String command = "cmd /c start javadoc -encoding UTF-8 -charset UTF-8 " + fileLocation + " -d" + " " + dir.getAbsolutePath();
             System.out.println("command = " + command);
             Runtime.getRuntime().exec(command);
         } catch (IOException e) {
             System.out.println("执行失败，请检查您的输入是否异常，或者jdk异常");
         }

     }


     /**
      * 查看ArrayList源码解析
      */
     public static void showArrayLitSource() {
         try {
             Runtime.getRuntime().exec("cmd /c start https://blog.csdn.net/qq_46283617/article/details/128518643");
         } catch (IOException e) {
             throw new RuntimeException(e);
         }
     }

     // 太过于冗余，下面进行重构
//     /**
//      * 冒泡排序动画
//      */
//     public static void showBubbleSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "bubble_sort.html";
//         System.out.println("location = " + location);
//         System.out.println(System.getProperty("user.dir"));
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 二分查找动画
//      */
//     public static void showBinarySearch(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "binary_search.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 插入排序动画
//      */
//     public static void showInsertSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "insertion_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 快速排序动画
//      */
//     public static void showQuickSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "quick_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 选择排序动画
//      */
//     public static void showSelectSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "selection_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 希尔排序动画
//      */
//     public static void showShellSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "shell_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }

     /**
      * 快捷开启各种排序
      * @param sortEnum
      */
     public static void showEachSort(SortEnum sortEnum) {

         String fileName = sortEnum.getFileName();
         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + fileName;
         try {
             Runtime.getRuntime().exec("cmd /c start " + location);
         } catch (IOException e) {
             throw new RuntimeException(e);
         }


     }

     /**
      * 乘法表 num
      * @param num
      */
     public static void showNumbMultiplyNumb(int num) {
         for (int i = 1; i <= num; i++) {
             for (int j = 1; j <= i; j++) {
                 System.out.printf("%-2d * %-2d = %-2d  ", i, j, i * j);
             }
             System.out.println();
         }
     }

     /**
      * 随机显示一张图片
      */
     public static void showBeauty() {
         // 获取连接信息
         Connection connection = Jsoup.connect("https://api.isoyu.com/mmnew_images.php").followRedirects(true).ignoreContentType(true);
         // 获取body
         String filename = "";
         try {
             // 获取流信息
             Connection.Response response = connection.method(Connection.Method.GET).execute();
             // 获取文件字节数据
             byte[] bytes = response.bodyAsBytes();
             long name = System.currentTimeMillis();
             File file = new File("img");
             // 在当前文件夹下面创建img存储图片
             if (!file.exists()) {
                 file.mkdirs();
             }

             filename = "img" + File.separator + name + ".jpg";
             try (var writer = new BufferedOutputStream(new FileOutputStream(filename))) {
                 // 文件保存到本地
                 writer.write(bytes);
             } catch (Exception ex) {
                 System.out.println("获取失败了~~~");
             }
         } catch (IOException e) {
             System.out.println("获取失败了~~~");
         }

         try {
             // 打开图片
             Runtime.getRuntime().exec("cmd /c start " + filename);
         } catch (IOException e) {
             System.out.println("获取失败了~~~");
         }


     }

     /**
      * 获取当前时间 时间格式yyyy-MM-dd hh:mm:ss
      * @return
      */
     public static String getLocalTime() {
         // 差强人意
//         LocalDateTime localDateTime = LocalDateTime.now();
//         return localDateTime.toString();
         long time = System.currentTimeMillis();
         return String.format("%tF %<tT", time);
     }

     /**
      * 猜测随机数 发现bug 在测试里面无法使用BufferedReader读取控制台内容
      * @return
      */
     public static boolean guessNumber() {
         // 随机生成一个数
         Random random = new Random();
         int i = random.nextInt(1, 1 << 12);
         // 获取读取控制台信息的流对象
         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         System.out.println("请输入您猜测的数字：");
         while (true) {
             int inputNum;
             try {
                 inputNum = Integer.parseInt(reader.readLine());
                 if (inputNum == 0) {
                     break;
                 }
             } catch (Exception e) {
                 System.out.println("您输入有误，请重新输入【输入0退出】:");
                 continue;
             }

             if (i == inputNum) {
                 System.out.println("恭喜你！猜测成功！");
                 return true;
             } else if (i > inputNum) {
                 System.out.println("您猜测的数字小了~【输入0退出】");
             } else {
                 System.out.println("您猜测的数字大了~【输入0退出】");
             }

         }

         return false;
     }

     /**
      * 运行hash演示动画
      */
     public static void showHashMapDemo() {
         try {
             Runtime.getRuntime().exec("cmd /k java -jar --add-opens java.base/jdk.internal.misc=ALL-UNNAMED other/hash-demo.jar");
             System.out.println("请稍等10s，正在为您打开示例网站~");
             Thread.sleep(5000);
             Runtime.getRuntime().exec("cmd /c start http://localhost:9998");
         } catch (Exception ex) {
             ex.printStackTrace();
         }
     }

     /**
      * 核心方法：
      *     二级菜单的选择还是有很多的问题
      *     当进入二级菜单，里面还有功能序号选择的话，无发正确的判断用户再输入的序号是第一层菜单的 还是第二层菜单的
      *     方法解决1：
      *         给第二层的菜单序号采用别的方法排序，比如a-z A-Z 之类的
      *         但是我感觉这种方法虽然简便一些，但是对用户不太友好。
      *      方法解决2：【测试 可行！】
      *         保存第一层菜单的合法数据，第二层菜单再选择时，拼接第一层的序号。
      *         这样当选择
      *             菜单1： 1
      *                 菜单2: 2
      *          拼接返回 12
      *             这样可以选择12进行功能的选择。
      *
      * @param numFirst 一级菜单的选择序号
      * @param numSecond 二级菜单的选择序号
      */
     public static int coreChoice(int numFirst, int numSecond) {
         StringBuilder builder = new StringBuilder();
         String str = builder.append(numFirst).append(numSecond).toString();
         try {
             return Integer.parseInt(str);
         }catch (Exception ex){
             System.out.println("系统错误，请检查输入的合法性！");
         }
         // 返回-1代表异常！
         return -1;
     }


     public static void main(String[] args) {
         guessNumber();
     }

 }
