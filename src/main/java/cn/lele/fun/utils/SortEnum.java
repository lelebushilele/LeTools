package cn.lele.fun.utils;

/**
 * <p>Project: LeTools - SortEnum
 * <p>Powered by wuyahan On 2023-01-13 08:52:57
 *
 * @author wuyahan [tianwenle2000@163.com]
 * @version 1.0
 * @since 17
 */
public enum SortEnum {
    BUBBLE(0, "bubble_sort.html"), BINARY(1, "binary_search.html"), INSERTION(2, "insertion_sort.html"), QUICK(3, "quick_sort.html"), SELECTION(4, "selection_sort.html"), SHELL(5, "shell_sort.html");




    SortEnum(int diff) {
        this.diff = diff;
    }

    private int diff;
    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    SortEnum(int diff, String fileName) {
        this.diff = diff;
        this.fileName = fileName;
    }


    public int getDiff() {
        return diff;
    }

    public void setDiff(int diff) {
        this.diff = diff;
    }

    public static String getFilenameByEnumId(int diff){
        for (SortEnum sortEnum : SortEnum.values()) {
            if (sortEnum.diff == diff) {
                return sortEnum.fileName;
            }
        }
        return "";
    }



    @Override
    public String toString() {
        return super.toString();
    }
}
