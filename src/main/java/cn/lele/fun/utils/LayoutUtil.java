 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun.utils;

 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStreamReader;
 import java.util.Scanner;

 /**
  * <p>Project: LeTools - LayoutUtil
  * <p>Powered by wuyahan On 2023-01-13 21:10:41
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class LayoutUtil {

     private static Scanner scanner = new Scanner(System.in);

     /**
      * 控制文本位置以及快捷输出
      *
      * @param text    要输出的文本
      * @param leftNum 距离左边多少空格
      * @param repeat  文本重复次数
      */
     public static void letTextInCenter(String text, int leftNum, int repeat) {
         System.out.println(" ".repeat(leftNum) + text.repeat(repeat));
     }

     /**
      * 一级菜单显示
      */
     public static void showMenu() {
         letTextInCenter("*", 0, 50);
         letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
         letTextInCenter("1.小程序初显神通", 20, 1);
         letTextInCenter("2.呆头鹅一片惘然", 20, 1);
         letTextInCenter("3.小小分支奈我何", 20, 1);
         letTextInCenter("4.各种排序盘前餐", 20, 1);
         letTextInCenter("5.看本源应千万变", 20, 1);
         letTextInCenter("6.看一看风景如画", 20, 1);
         letTextInCenter("7.娱乐放松不可能", 20, 1);
         letTextInCenter("8.双链集合最难懂", 20, 1);
         letTextInCenter("0.退 出  程  序", 20, 1);
         letTextInCenter("*", 0, 50);

     }

     /**
      * 耳二级菜单显示
      * @param num 以及菜单对应的编号
      */
     public static void menuDeepFirst(int num) {

         Scanner scanner = new Scanner(System.in);

         switch (num) {
             case 0->System.exit(0);
             case 1 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.词典翻译",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 2 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.文档生成",20,1);
                 letTextInCenter("2.获取时间",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 3 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.打印乘法表",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 4 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.冒泡排序动画",20,1);
                 letTextInCenter("2.插入排序动画",20,1);
                 letTextInCenter("3.快 排 动 画",20,1);
                 letTextInCenter("4.选择排序动画",20,1);
                 letTextInCenter("5.希尔排序动画",20,1);
                 letTextInCenter("6.二分查找动画",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 5 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.ArrayList源码解析",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 6->{
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.随机打开美图",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 7->{
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.猜数游戏神魔本",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 8->{
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.查看HashDemo",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             default -> {
                 // 如果选择错误的序号 重新打印菜单 提示输入
                 showMenu();
                 System.out.println("default: 输入有误，请重新输入!");
                 // 如果用户 执迷不悟，那就把-1传进去 继续走default输入
                 int i = -1;
                 try {
                     i = scanner.nextInt();
                 } catch (Exception e) {
//                     System.out.println("输入有误，请重新输入！");
                 }
                 menuDeepFirst(i);
             }
         }

     }

     public static void functionShow(int num){
         switch (num){
             case 11-> {
                 System.out.println("请输入您要翻译的英文或者中文：");
                 String word = scanner.nextLine();
                 System.out.println("翻译结果：");
                 System.out.println(MiddleController.getWordsChineseOrEnglish(word));
             }
             case 21-> System.out.println("该功能已放弃~");
             case 22-> {
                 System.out.println("正在获取当前时间：");
                 String localTime = MiddleController.getLocalTime();
                 System.out.println(localTime);
             }
             case 31->{
                 System.out.println("请问您要打印几乘几的乘法表：【1-101】");
                 int i = scanner.nextInt();
                 MiddleController.showNumbMultiplyNumb(i);
             }
             case 41 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.BUBBLE);
             }
             case 42 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.INSERTION);
             }
             case 43 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.QUICK);
             }
             case 44 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.SELECTION);
             }
             case 45 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.SHELL);
             }
             case 46 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.BINARY);
             }
             case 51->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showArrayLitSource();
             }
             case 61->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showBeauty();
             }
             case 71->{
                 MiddleController.guessNumber();
             }
             case 81->{
                 MiddleController.showHashMapDemo();
             }
         }
     }

     public static void main(String[] args) throws IOException {
         menuDeepFirst(21);
     }

 }
