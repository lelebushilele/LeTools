 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun;

 import cn.lele.fun.utils.LayoutUtil;

 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStreamReader;

 import static cn.lele.fun.utils.LayoutUtil.*;
 import static cn.lele.fun.utils.MiddleController.coreChoice;

 /**
  * <p>Project: LeTools - LeToolsApplication
  * <p>Powered by wuyahan On 2023-01-12 20:37:47
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class LeToolsApplication {

     /**
      * Description: 方法入口，这里进行方法的显示
      *
      * @param args:
      * @date: 2023/1/13 21:53
      * @params
      */
     public static void main(String[] args) {
         // 显示菜单
         showMenu();
         int num = 0;
         try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
             while (true) {
                 try {
                     System.out.println("请输入您要选择的序号：");
                     // 第一层菜单的选择
                     num = Integer.parseInt(reader.readLine());
                     // 进入二级菜单
                     shoeRepeatSecMenu(num, reader);

                 } catch (Exception e) {
                     System.out.println("输入错误的口令！为您打开首页。");
                     showMenu();
                 }

             }

         } catch (Exception e) {
             e.printStackTrace();
             System.out.println("输入错误，请重新输入！");
         }


     }

     // 二级菜单功能演示以后，继续打印二级菜单
     // 方便用户回以及菜单
     public static void shoeRepeatSecMenu(int num, BufferedReader reader) throws IOException {
         // 二级菜单的调度
         menuDeepFirst(num);
         System.out.println("请输入您要选择的序号：");
         // 第二层菜单的选择
         int numSec = Integer.parseInt(reader.readLine());
         if (numSec == 0){
             showMenu();
//             System.out.println("请输入您要选择的序号：");
         }else {
             // core code
             // 先去做功能菜单的调用
             int i = coreChoice(num, numSec);
             // 这里运行完毕后
             functionShow(i);
             // 再次调用二级菜单
             shoeRepeatSecMenu(num,reader);

         }
     }

 }
