# 开发过程

## 具体思路

`实在是不想去写那些黑框框了！`正好最近学了很多的东西，打算把所有的东西从头开始进行总结一些。

然后，做成一个**黑框框~~**  需要添加的功能我先放在下面，后面有灵感了继续添加。

**第一章：**

1. 词典翻译软件 （加强 plus pro maxmax 版本）【完成】
2. 万能搜索软件 //无法绕过验证【放弃】
3. 百度热搜榜单/微博热搜榜单 【暂时放弃】

**第二章：**

1. 文档生成（简易版本 ）【放弃】
2. 获取当前各种时间（）【算是完成】

**第三章：**

1. 打印乘法表 【完成】
2. 待补充（实在想不到了）

**第四章：**

1. 冒泡排序动画 【完成】
2. 插入排序动画【完成】
3. 快  排   动   画【完成】
4. 选择排序动画【完成】
5. 希尔排序动画【完成】
6. 二分查找动画【完成】

**第五章：**

1. ArrayList源码解析【完成】
2. 待添加

**第六章：**

1. 随机打开美图【完成】

**第七章：**

1. 猜数游戏一百万版本【完成】

**第八章：**

 	1.    运行hash_demo 【完成】

## 开发前准备

添加依赖信息

```xml
<dependencies>
    <!--爬取网页信息-->
    <dependency>
        <groupId>org.jsoup</groupId>
        <artifactId>jsoup</artifactId>
        <version>1.15.3</version>
    </dependency>
    <!--方便测试-->
    <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter-engine</artifactId>
        <version>5.9.0</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

在这里发现bug一枚:`在测试里面无法使用BufferedReader读取控制台内容。`

完成功能任务编写：

```java
 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun.utils;

 /**
  * <p>Project: LeTools - MiddleController
  * <p>Powered by wuyahan On 2023-01-12 20:34:11
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */

 import org.jsoup.Connection;
 import org.jsoup.Jsoup;
 import org.jsoup.nodes.Document;
 import org.jsoup.nodes.Element;
 import org.jsoup.select.Elements;

 import java.io.*;
 import java.time.LocalDateTime;
 import java.util.Arrays;
 import java.util.Random;

 /** 中心方法 总控全局
  * <p>Powered by wuyahan on 2023-01-12 20:34</p>
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since
  */
 public class MiddleController {

     /*
      * Description: 翻译软件 增强区分
      * @date: 2023/1/12 21:41
      * @params
      * @param word:
      * @return java.lang.String
      */
     public static String getWordsChineseOrEnglish(String word) {
         String ydUrl = "https://youdao.com/result?word=%s&lang=en";
         // 获取连接对象
         Connection connection = Jsoup.connect(String.format(ydUrl, word)).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36");
         connection.followRedirects(true);
         connection.ignoreContentType(true);
         String text = "";
         try {
             // 正常情况下的翻译区域
             text = connection.get().getElementsByClass("basic").get(0).text();
             // 英文翻译
             if (text.matches("[A-Za-z]+")) {
                 text = text.replaceAll("\\s\\d", "\r\n$0");
             } else {
                 // 非英文翻译
                 text = text.replace("；", "\r\n");
             }
             return text;
         } catch (Exception e) {
             System.out.println("您输入是否有误？小的搜不到啊~");
         }
         return text;
     }

     /*
      * Description: 搜索尽可能搜索到的 这个先放弃 无法绕过验证
      * @date: 2023/1/12 21:41
      * @params
      * @param word:
      * @return java.lang.String
      */
     public static String getAllInThisWord(String word) {
         // 连接地址
         String bdUrl = "https://baike.baidu.com/item/%s";
         // 获取连接对象
//         Connection connection = Jsoup.connect(String.format(bdUrl,word)).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36").followRedirects(true).ignoreContentType(true);
         Connection connection = Jsoup.connect(String.format(bdUrl, word));
         // 做伪装
//         connection.header("Host","baike.baidu.com");
//         connection.header("Referer",String.format(bdUrl,word));
//         connection.header("","");
         // 获取内容
         try {

             Document document = connection.get();
             System.out.println("document = " + document);

             Elements elements = connection.get().select("div[class=lemma-summary]");
             for (Element element : elements) {
                 System.out.println("element = " + element);
             }
//             System.out.println("text = " + text);
         } catch (IOException e) {
             throw new RuntimeException(e);
         }

         return "";
     }

     /**
      * 生成java说明文档
      * @param fileLocation
      */
     public static void createJavaDocByFileLocations(String fileLocation) {
         if (!fileLocation.endsWith("java")) {
             System.out.println("对不起输入的地址不正确");
             return;
         }
         File file = new File(fileLocation);
         if (file.isDirectory() || !file.exists()) {
             System.out.println("请检查您输入的地址是否有误~");
             return;
         }

         File dir = new File(System.getProperty("user.dir") + File.separator + "api");
         if (!dir.exists()) {
             dir.mkdirs();
         }

         System.out.println(dir.getAbsolutePath());

         // 执行命令生成文档
         try {
             String command = "cmd /c start javadoc -encoding UTF-8 -charset UTF-8 " + fileLocation + " -d" + " " + dir.getAbsolutePath();
             System.out.println("command = " + command);
             Runtime.getRuntime().exec(command);
         } catch (IOException e) {
             System.out.println("执行失败，请检查您的输入是否异常，或者jdk异常");
         }

     }


     /**
      * 查看ArrayList源码解析
      */
     public static void showArrayLitSource() {
         try {
             Runtime.getRuntime().exec("cmd /c start https://blog.csdn.net/qq_46283617/article/details/128518643");
         } catch (IOException e) {
             throw new RuntimeException(e);
         }
     }

     // 太过于冗余，下面进行重构
//     /**
//      * 冒泡排序动画
//      */
//     public static void showBubbleSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "bubble_sort.html";
//         System.out.println("location = " + location);
//         System.out.println(System.getProperty("user.dir"));
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 二分查找动画
//      */
//     public static void showBinarySearch(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "binary_search.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 插入排序动画
//      */
//     public static void showInsertSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "insertion_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 快速排序动画
//      */
//     public static void showQuickSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "quick_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 选择排序动画
//      */
//     public static void showSelectSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "selection_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//     /**
//      * 希尔排序动画
//      */
//     public static void showShellSort(){
//         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + "shell_sort.html";
//         try {
//             Runtime.getRuntime().exec("cmd /c start " + location);
//         } catch (IOException e) {
//             throw new RuntimeException(e);
//         }
//     }

     /**
      * 快捷开启各种排序
      * @param sortEnum
      */
     public static void showEachSort(SortEnum sortEnum) {

         String fileName = sortEnum.getFileName();
         String location = System.getProperty("user.dir") + File.separator + "other" + File.separator + fileName;
         try {
             Runtime.getRuntime().exec("cmd /c start " + location);
         } catch (IOException e) {
             throw new RuntimeException(e);
         }


     }

     /**
      * 乘法表 num
      * @param num
      */
     public static void showNumbMultiplyNumb(int num) {
         for (int i = 1; i <= num; i++) {
             for (int j = 1; j <= i; j++) {
                 System.out.printf("%-2d * %-2d = %-2d  ", i, j, i * j);
             }
             System.out.println();
         }
     }

     /**
      * 随机显示一张图片
      */
     public static void showBeauty() {
         // 获取连接信息
         Connection connection = Jsoup.connect("https://api.isoyu.com/mmnew_images.php").followRedirects(true).ignoreContentType(true);
         // 获取body
         String filename = "";
         try {
             // 获取流信息
             Connection.Response response = connection.method(Connection.Method.GET).execute();
             // 获取文件字节数据
             byte[] bytes = response.bodyAsBytes();
             long name = System.currentTimeMillis();
             File file = new File("img");
             // 在当前文件夹下面创建img存储图片
             if (!file.exists()) {
                 file.mkdirs();
             }

             filename = "img" + File.separator + name + ".jpg";
             try (var writer = new BufferedOutputStream(new FileOutputStream(filename))) {
                 // 文件保存到本地
                 writer.write(bytes);
             } catch (Exception ex) {
                 System.out.println("获取失败了~~~");
             }
         } catch (IOException e) {
             System.out.println("获取失败了~~~");
         }

         try {
             // 打开图片
             Runtime.getRuntime().exec("cmd /c start " + filename);
         } catch (IOException e) {
             System.out.println("获取失败了~~~");
         }


     }

     /**
      * 获取当前时间 时间格式yyyy-MM-dd hh:mm:ss
      * @return
      */
     public static String getLocalTime() {
         // 差强人意
//         LocalDateTime localDateTime = LocalDateTime.now();
//         return localDateTime.toString();
         long time = System.currentTimeMillis();
         return String.format("%tF %<tT", time);
     }

     /**
      * 猜测随机数 发现bug 在测试里面无法使用BufferedReader读取控制台内容
      * @return
      */
     public static boolean guessNumber() {
         // 随机生成一个数
         Random random = new Random();
         int i = random.nextInt(1, 1 << 12);
         // 获取读取控制台信息的流对象
         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         System.out.println("请输入您猜测的数字：");
         while (true) {
             int inputNum;
             try {
                 inputNum = Integer.parseInt(reader.readLine());
                 if (inputNum == 0) {
                     break;
                 }
             } catch (Exception e) {
                 System.out.println("您输入有误，请重新输入【输入0退出】:");
                 continue;
             }

             if (i == inputNum) {
                 System.out.println("恭喜你！猜测成功！");
                 return true;
             } else if (i > inputNum) {
                 System.out.println("您猜测的数字小了~【输入0退出】");
             } else {
                 System.out.println("您猜测的数字大了~【输入0退出】");
             }

         }

         return false;
     }

     /**
      * 运行hash演示动画
      */
     public static void showHashMapDemo() {
         try {
             Runtime.getRuntime().exec("cmd /k java -jar --add-opens java.base/jdk.internal.misc=ALL-UNNAMED other/hash-demo.jar");
             System.out.println("请稍等10s，正在为您打开示例网站~");
             Thread.sleep(5000);
             Runtime.getRuntime().exec("cmd /c start http://localhost:9998");
         } catch (Exception ex) {
             ex.printStackTrace();
         }
     }

     /**
      * 核心方法：
      *     二级菜单的选择还是有很多的问题
      *     当进入二级菜单，里面还有功能序号选择的话，无发正确的判断用户再输入的序号是第一层菜单的 还是第二层菜单的
      *     方法解决1：
      *         给第二层的菜单序号采用别的方法排序，比如a-z A-Z 之类的
      *         但是我感觉这种方法虽然简便一些，但是对用户不太友好。
      *      方法解决2：【测试 可行！】
      *         保存第一层菜单的合法数据，第二层菜单再选择时，拼接第一层的序号。
      *         这样当选择
      *             菜单1： 1
      *                 菜单2: 2
      *          拼接返回 12
      *             这样可以选择12进行功能的选择。
      *
      * @param numFirst 一级菜单的选择序号
      * @param numSecond 二级菜单的选择序号
      */
     public static int coreChoice(int numFirst, int numSecond) {
         StringBuilder builder = new StringBuilder();
         String str = builder.append(numFirst).append(numSecond).toString();
         try {
             return Integer.parseInt(str);
         }catch (Exception ex){
             System.out.println("系统错误，请检查输入的合法性！");
         }
         // 返回-1代表异常！
         return -1;
     }


     public static void main(String[] args) {
         guessNumber();
     }

 }

```

完成布局代码编写：

```java
 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun.utils;

 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStreamReader;
 import java.util.Scanner;

 /**
  * <p>Project: LeTools - LayoutUtil
  * <p>Powered by wuyahan On 2023-01-13 21:10:41
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class LayoutUtil {

     private static Scanner scanner = new Scanner(System.in);

     /**
      * 控制文本位置以及快捷输出
      *
      * @param text    要输出的文本
      * @param leftNum 距离左边多少空格
      * @param repeat  文本重复次数
      */
     public static void letTextInCenter(String text, int leftNum, int repeat) {
         System.out.println(" ".repeat(leftNum) + text.repeat(repeat));
     }

     /**
      * 一级菜单显示
      */
     public static void showMenu() {
         letTextInCenter("*", 0, 50);
         letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
         letTextInCenter("1.小程序初显神通", 20, 1);
         letTextInCenter("2.呆头鹅一片惘然", 20, 1);
         letTextInCenter("3.小小分支奈我何", 20, 1);
         letTextInCenter("4.各种排序盘前餐", 20, 1);
         letTextInCenter("5.看本源应千万变", 20, 1);
         letTextInCenter("6.看一看风景如画", 20, 1);
         letTextInCenter("7.娱乐放松不可能", 20, 1);
         letTextInCenter("8.双链集合最难懂", 20, 1);
         letTextInCenter("0.退 出  程  序", 20, 1);
         letTextInCenter("*", 0, 50);

     }

     /**
      * 耳二级菜单显示
      * @param num 以及菜单对应的编号
      */
     public static void menuDeepFirst(int num) {

         Scanner scanner = new Scanner(System.in);

         switch (num) {
             case 0->System.exit(0);
             case 1 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.词典翻译",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 2 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.文档生成",20,1);
                 letTextInCenter("2.获取时间",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 3 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.打印乘法表",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 4 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.冒泡排序动画",20,1);
                 letTextInCenter("2.插入排序动画",20,1);
                 letTextInCenter("3.快 排 动 画",20,1);
                 letTextInCenter("4.选择排序动画",20,1);
                 letTextInCenter("5.希尔排序动画",20,1);
                 letTextInCenter("6.二分查找动画",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 5 -> {
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.ArrayList源码解析",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 6->{
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.随机打开美图",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 7->{
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.猜数游戏神魔本",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             case 8->{
                 letTextInCenter("*", 0, 50);
                 letTextInCenter("当前时间：" + MiddleController.getLocalTime(), 15, 1);
                 letTextInCenter("1.查看HashDemo",20,1);
                 letTextInCenter("0.返回上一层", 20, 1);
                 letTextInCenter("*", 0, 50);
             }
             default -> {
                 // 如果选择错误的序号 重新打印菜单 提示输入
                 showMenu();
                 System.out.println("default: 输入有误，请重新输入!");
                 // 如果用户 执迷不悟，那就把-1传进去 继续走default输入
                 int i = -1;
                 try {
                     i = scanner.nextInt();
                 } catch (Exception e) {
//                     System.out.println("输入有误，请重新输入！");
                 }
                 menuDeepFirst(i);
             }
         }

     }

     public static void functionShow(int num){
         switch (num){
             case 11-> {
                 System.out.println("请输入您要翻译的英文或者中文：");
                 String word = scanner.nextLine();
                 System.out.println("翻译结果：");
                 System.out.println(MiddleController.getWordsChineseOrEnglish(word));
             }
             case 21-> System.out.println("该功能已放弃~");
             case 22-> {
                 System.out.println("正在获取当前时间：");
                 String localTime = MiddleController.getLocalTime();
                 System.out.println(localTime);
             }
             case 31->{
                 System.out.println("请问您要打印几乘几的乘法表：【1-101】");
                 int i = scanner.nextInt();
                 MiddleController.showNumbMultiplyNumb(i);
             }
             case 41 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.BUBBLE);
             }
             case 42 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.INSERTION);
             }
             case 43 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.QUICK);
             }
             case 44 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.SELECTION);
             }
             case 45 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.SHELL);
             }
             case 46 ->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showEachSort(SortEnum.BINARY);
             }
             case 51->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showArrayLitSource();
             }
             case 61->{
                 System.out.println("请稍等，正在为您打开...");
                 MiddleController.showBeauty();
             }
             case 71->{
                 MiddleController.guessNumber();
             }
             case 81->{
                 MiddleController.showHashMapDemo();
             }
         }
     }

     public static void main(String[] args) throws IOException {
         menuDeepFirst(21);
     }

 }

```

为了方便对冗余代码的管理，特意准备了枚举类

```java
package cn.lele.fun.utils;

/**
 * <p>Project: LeTools - SortEnum
 * <p>Powered by wuyahan On 2023-01-13 08:52:57
 *
 * @author wuyahan [tianwenle2000@163.com]
 * @version 1.0
 * @since 17
 */
public enum SortEnum {
    BUBBLE(0, "bubble_sort.html"), BINARY(1, "binary_search.html"), INSERTION(2, "insertion_sort.html"), QUICK(3, "quick_sort.html"), SELECTION(4, "selection_sort.html"), SHELL(5, "shell_sort.html");




    SortEnum(int diff) {
        this.diff = diff;
    }

    private int diff;
    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    SortEnum(int diff, String fileName) {
        this.diff = diff;
        this.fileName = fileName;
    }


    public int getDiff() {
        return diff;
    }

    public void setDiff(int diff) {
        this.diff = diff;
    }

    public static String getFilenameByEnumId(int diff){
        for (SortEnum sortEnum : SortEnum.values()) {
            if (sortEnum.diff == diff) {
                return sortEnum.fileName;
            }
        }
        return "";
    }



    @Override
    public String toString() {
        return super.toString();
    }
}

```

启动代码：

```java
 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package cn.lele.fun;

 import cn.lele.fun.utils.LayoutUtil;

 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStreamReader;

 import static cn.lele.fun.utils.LayoutUtil.*;
 import static cn.lele.fun.utils.MiddleController.coreChoice;

 /**
  * <p>Project: LeTools - LeToolsApplication
  * <p>Powered by wuyahan On 2023-01-12 20:37:47
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class LeToolsApplication {

     /**
      * Description: 方法入口，这里进行方法的显示
      *
      * @param args:
      * @date: 2023/1/13 21:53
      * @params
      */
     public static void main(String[] args) {
         // 显示菜单
         showMenu();
         int num = 0;
         try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
             while (true) {
                 try {
                     System.out.println("请输入您要选择的序号：");
                     // 第一层菜单的选择
                     num = Integer.parseInt(reader.readLine());
                     // 进入二级菜单
                     shoeRepeatSecMenu(num, reader);

                 } catch (Exception e) {
                     System.out.println("输入错误的口令！为您打开首页。");
                     showMenu();
                 }

             }

         } catch (Exception e) {
             e.printStackTrace();
             System.out.println("输入错误，请重新输入！");
         }


     }

     // 二级菜单功能演示以后，继续打印二级菜单
     // 方便用户回以及菜单
     public static void shoeRepeatSecMenu(int num, BufferedReader reader) throws IOException {
         // 二级菜单的调度
         menuDeepFirst(num);
         System.out.println("请输入您要选择的序号：");
         // 第二层菜单的选择
         int numSec = Integer.parseInt(reader.readLine());
         if (numSec == 0){
             showMenu();
//             System.out.println("请输入您要选择的序号：");
         }else {
             // core code
             // 先去做功能菜单的调用
             int i = coreChoice(num, numSec);
             // 这里运行完毕后
             functionShow(i);
             // 再次调用二级菜单
             shoeRepeatSecMenu(num,reader);

         }
     }

 }

```

## 部分功能展示

打开随机图片：

![image-20230114111246778](https://gitee.com/lelebushilele/img_note/raw/master/img/2022/20230114111252.png)

部分功能展示：

![image-20230114110048389](https://gitee.com/lelebushilele/img_note/raw/master/img/2022/20230114110055.png)

## 配置maven打包插件

```xml
 <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>2.4.1</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>cn.lele.fun.LeToolsApplication</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>
```

打包结果：

![image-20230114113135930](https://gitee.com/lelebushilele/img_note/raw/master/img/2022/20230114113141.png)

运行结果：

![image-20230114113211709](https://gitee.com/lelebushilele/img_note/raw/master/img/2022/20230114113215.png)